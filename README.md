# nodejs-file-upload-server
A Node server exclusively developed with Node.js, Express, mongoDB, and Multer, built solely for file uploading task.

```
$ git clone https://github.com/SinghDigamber/nodejs-file-upload-server.git
```

## Installation
```
$ cd nodejs-file-upload-server
$ npm install
```

## Run MySQL Database
```
$sudo apt update
$sudo apt install mysql-server
$sudo mysql_secure_installation
```

## Run Nodemon
```
$ nodemon
```
