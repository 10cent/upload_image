let express = require('express'),
  multer = require('multer'),
  fs = require('fs'),
  router = express.Router();
  const { models } = require("../models/db")
 
// Multer File upload settings
const DIR = './public/';
const appConstant = require('../app_constant')
const {
  USER, IMAGE
} = appConstant.DATA_TABLE
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    cb(null, fileName)
  }
});

// Multer Mime Type Validation
var upload = multer({
  storage: storage,
  // limits: {
  //   fileSize: 1024 * 1024 * 5
  // },
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    }
  }
});


// User model

// POST User
router.post('/create-user', upload.single('avatar'), (req, res, next) => {
  const url = req.protocol + '://' + req.get('host')
  models[USER].create({
    name: req.body.name,
    avatar: url + '/public/' + req.file.filename
  }).then(result => {
    res.status(201).json({
      message: "User registered successfully!",
      userCreated: {
        _id: result.id,
        name: result.name,
        avatar: result.avatar
      }
    })
  }).catch(err => {
    console.log(err),
      res.status(500).json({
        error: err
      });
  })
})

// GET All Users
router.get("/", (req, res, next) => {
  User.find().then(data => {
    res.status(200).json({
      message: "Users retrieved successfully!",
      users: data
    });
  });
});

// post image using fs
router.post('/update-image', async (req, res) => {
  console.log("aaaa");
  const url = req.protocol + '://' + req.get('host')
  const image = await uploadToDisk(`${DIR}/${req.body.filename}`,req.body.base64_string)
  models[USER].create({
    name: req.body.name,
    avatar: url + '/public/' + req.body.filename
  }).then(result => {
    console.log(result);
    res.status(201).json({
      message: "User registered successfully!",
      userCreated: {
        _id: result.id,
        name: result.name,
        avatar: result.avatar
      }
    })
  }).catch(err => {
    console.log(err),
      res.status(500).json({
        error: err
      });
  })
}
 )

async function uploadToDisk(path, base64String) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, base64String, 'base64', (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(path);
      }
    });
  })
}

module.exports = router;
