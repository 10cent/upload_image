module.exports = Object.freeze({
    db: {
        database: 'angular_formdata',
        username: 'root',
        password: '12345678',
        options: {
            host: '127.0.0.1',
            port: 3306,
            dialect: 'mysql',
            define: {
                charset: 'utf8',
                collate: 'utf8_general_ci',
                underscored: true,
            },
            sync: { force: false, alter: false },
            timestamps: true,
            logging: false
        }
    }
});