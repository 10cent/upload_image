
function getHooks() {
    return {
        beforeValidate: (user) => {
            if (typeof user.email === 'string') {
                user.email = user.email.toLowerCase();
            }
        }
    }
}
module.exports = {
    getHooks
}
