

function getSchema(DataTypes) {
    return {
        id: {
            type: DataTypes.INTEGER(9).ZEROFILL.UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        avatar: {
            type: DataTypes.STRING
        }
    };
}

module.exports = {
    getSchema
}
