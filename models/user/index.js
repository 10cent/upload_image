
const userSchema = require('./user.schema')
const userClassMethods = require('./user.classMethods');
const userHook = require('./user.hook')

const hooks = userHook.getHooks()
const { USER } = require('../../app_constant').DATA_TABLE

module.exports = (sequelize, DataTypes) => {
    const schema = userSchema.getSchema(DataTypes)
    const User = userClassMethods.init(
        schema,
        {
            timestamps: true,
            updatedAt: 'updated_at',
            createdAt: 'created_at',
            hooks,
            sequelize,
            modelName: USER,
            tableName: USER
        }
    );
    return User;
}
