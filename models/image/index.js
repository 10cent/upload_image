
const imageSchema = require('./image.schema')
const imageClassMethods = require('./image.classMethods');
const imageHook = require('./image.hook')
const { IMAGE } = require("../../app_constant").DATA_TABLE
const hooks = imageHook.getHooks()

module.exports = (sequelize, DataTypes) => {
  const schema = imageSchema.getSchema(DataTypes);
  const Tracking = imageClassMethods.init(
    schema,
    {
      timestamps: true,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
      underscored: true,
      hooks,
      sequelize,
      modelName: IMAGE,
      tableName: IMAGE
    }
  );
  return Tracking;
}
