function getSchema(DataTypes) {
    return {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        url: {
            type: DataTypes.STRING,
        }
    };
}

module.exports = {
    getSchema
}
