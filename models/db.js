
const Sequelize = require('sequelize');
const config = require('../config');

const appConstant = require('../app_constant')
const {
    database, username, password, options
} = config.db;

const {
USER, IMAGE
} = appConstant.DATA_TABLE
const models = {};
const sequelize = new Sequelize(database, username, password, options);


function getOrder(object) {
    if (!object) {
        object = {};
    }
    object.order = [
        ['created_at', 'DESC']
    ]
    return object;
}

function connect() {
    console.log('connect start');
    initModels();
    return sequelize.authenticate()
        .then(() => {
            return sequelize.sync()
        }).then(() => {

        }).catch((error) => {
            console.log(error);
            throw error;
        })
}

// merge the properties from models into this Db object
function initModels() {
    models[USER] = sequelize.import('./user');
    models[IMAGE] = sequelize.import('./image');
    Object.values(models)
        .filter(model => typeof model.associate === 'function')
        .forEach(model => model.associate(models));
}


function rawQuery(sql, whereClause) {
    return sequelize.query(sql, whereClause).then(result => {
        return result;
    })
}
module.exports = {
    connect,
    rawQuery,
    getOrder,
    sequelize,
    Sequelize,
    models
}
